import _ from 'lodash'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import YTSearch from 'youtube-api-search'

import SearchBar from './components/search_bar'
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'

const API_KEY = 'AIzaSyBc-s_7q6mgwqRuRdzbnLHZAAiV0G9OQD8'

class App extends Component {
    constructor(props) {
        super(props)

        this.state = { videos: [], selectedVideo: null }

        this.videoSearch('surfboards')
    }

    videoSearch(term) {
        YTSearch({ key: API_KEY, term }, (videos) => {
            this.setState({ videos, selectedVideo: videos[0] })
        })
    }

    render() {
        const videoDetails = this.state.selectedVideo
            ? <VideoDetail video={this.state.selectedVideo} />
            : <div>Loading...</div>
        const onSearchTermChange = _.debounce(this.videoSearch.bind(this), 500)

        return (
            <div>
                <SearchBar onSearchTermChange={onSearchTermChange} />
                <div className="row">
                    {videoDetails}
                    <VideoList
                    onVideoSelect={(video) => this.setState({selectedVideo: video})}
                    videos={this.state.videos} />
                </div>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.querySelector('.container'))
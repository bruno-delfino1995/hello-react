import React, { Component } from 'react'

export default class SearchBar extends Component {
    constructor(props) {
        super(props)

        this.state = { term: '' }
    }

    onInputChange(val) {
        this.setState({ term: val })
        this.props.onSearchTermChange(val);
    }

    render() {
        return (
            <div className="search-bar">
                <input type="text"
                value={this.state.term}
                onChange={(evt) => this.onInputChange(evt.target.value)}
                />
            </div>
        )
    }
}
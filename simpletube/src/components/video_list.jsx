import React from 'react'
import VideoItem from './video_list_item'

export default ({ videos, onVideoSelect }) => {
    const videoItems = videos.map((v) => {
        return (
            <VideoItem key={v.etag} video={v} onVideoSelect={onVideoSelect}/>
        )
    })
    
    return (
        <ul className="col-md-4 list-group">{videoItems}</ul>
    )
}
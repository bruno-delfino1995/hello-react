import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import moment from 'moment'

import { submitOrder } from '~/actions'
import TextArea from '~/utils/components/form/text-area'
import Input from '~/utils/components/form/input'
import Checkbox from '~/utils/components/form/checkbox'

class OrderSubmission extends Component {
    constructor(props) {
        super(props)
    }

    onSubmit(values) {
        this.props.submitOrder(values)
            .then((val) => {
                this.props.reset()
                return val
            })
    }

    render() {
        const { handleSubmit } = this.props

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))} id="order">
                <Field
                label="Date:"
                id="order_date"
                name="date"
                type="date"
                component={Input}
                />

                <Field
                label="Channel:"
                id="order_channel"
                name="channel"
                type="text"
                component={Input}
                />

                <Field
                label="Status"
                id="order_status"
                name="status"
                component={Checkbox}
                />

                <Field
                label="Description:"
                id="order_description"
                name="description"
                rows="4"
                component={TextArea}
                />

                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        )
    }
}

function validate(values) {
    const errors = {}

    if (!values.description) {
        errors.description = "You must have a description"
    } else if (values.description.length < 20 || values.description.length > 100) {
        errors.description = "Descriptions should have be between 20 and 100 characters"
    }

    if (!values.channel) {
        errors.channel = "You must specify a channel"
    }

    if (!values.date) {
        errors.date = "You must specify a date"
    } else if (!isBefore(moment(values.date), moment())) {
        errors.date = "Specify a date before today"
    }

    return errors
}

function isBefore(date, limit) {
    return date.valueOf() < limit.valueOf()
}

const ReduxContainer = connect(null, { submitOrder })(OrderSubmission)
export default reduxForm({
    validate,
    form: 'OrderSubmission'
})(ReduxContainer)


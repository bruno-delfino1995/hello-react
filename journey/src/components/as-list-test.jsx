import React, { Component } from 'react'
import { connect } from 'react-redux'

import AsList from '~/utils/components/as-list'
import WithProps from '~/utils/components/with-props'

import {
    fetchOrders as index,
    updateOrder as update,
    deleteOrder as del
} from '~/actions'

import Outer from './order-list'
import Inner from './order-list/item'

class AsListTest extends Component {
    constructor(props) {
        super(props)

        this.state = { canEdit: true }

        const ControlledInner = WithProps({
            update: this.props.update.bind(this),
            beforeUpdate: this.beforeUpdate.bind(this),
            afterUpdate: this.afterUpdate.bind(this),
            delete: this.props.delete.bind(this)
        }, Inner)

        this.List = AsList(Outer, ControlledInner)
    }

    beforeUpdate() {
        console.log('Before update')
        this.setState({ canEdit: false })
    }

    afterUpdate() {
        console.log('After update')
        this.setState({ canEdit: true })
    }

    render() {
        console.log('Rerendering the test')
        const List = this.List

        return (
            <List
            data={this.props.data} index={this.props.index.bind(this)}
            itemProps={() => ({ canEdit: this.state.canEdit })}
            />
        )
    }
}

export default connect(
    ({ orders }) => ({ data: orders }),
    { index, update, delete: del }
)(AsListTest)

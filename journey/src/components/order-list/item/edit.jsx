import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import moment from 'moment'

import { updateOrder } from '~/actions'
import Input from '~/utils/components/form/input'

class Edit extends Component {
    onSubmit(values) {
        this.props.updateOrder(this.props.order.id, values)
            .then((x) => {
                this.props.onSubmit()
                return x
            })
    }

    render() {
        const { order, handleSubmit, onCancel } = this.props

        return (
            <div>
                <form id="edit-order" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <div className="float-right">
                        <div>
                            <button
                            className="btn btn-success"
                            type="submit"
                            >
                                Submit
                            </button>
                        </div>
                        <div className="upper-space">
                            <button
                            className="btn btn-warning"
                            onClick={onCancel}
                            >
                                Cancel
                            </button>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col">
                            <Field
                            label="Channel:"
                            id="edit-order_channel"
                            name="channel"
                            type="text"
                            component={Input}
                            />
                        </div>
                        <div className="col">
                            <Field
                            label="Date:"
                            id="edit-order_date"
                            name="date"
                            type="date"
                            component={Input}
                            />
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

function validate(values) {
    const errors = {}

    if (!values.channel) {
        errors.channel = "You must specify a channel"
    }

    if (!values.date) {
        errors.date = "You must specify a date"
    } else if (!isBefore(moment(values.date), moment())) {
        errors.date = "Specify a date before today"
    }

    return errors
}

function isBefore(date, limit) {
    return date.valueOf() < limit.valueOf()
}

const EditForm = reduxForm({
    validate,
})(Edit)

function mapStateToProps(_state, { order }) {
    return {
        form: `OrderEdition-${order.id}`
    }
}

export default connect(mapStateToProps, { updateOrder })(EditForm)
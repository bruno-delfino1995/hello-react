import React from 'react'
import { Link } from 'react-router-dom'

export default function Show({ order, onDelete, onEdit }) {
    return (
        <div className="row">
            <div className="col-9 break-words">
                <Link to={`/orders/${order.id}`}>
                    {order.channel} - {order.date}
                </Link>
            </div>
            <div className="col-3">
                <div>
                    <button
                    className="btn btn-danger"
                    onClick={onDelete}
                    >
                        Delete
                    </button>
                </div>
                <div className="upper-space">
                    <button
                    className="btn btn-warning"
                    onClick={onEdit}
                    >
                        Edit
                    </button>
                </div>
            </div>
        </div>
    )
}
import { omit, assign } from 'lodash'

import { FETCH_ORDERS, SUBMIT_ORDER, FETCH_ORDER, DELETE_ORDER, UPDATE_ORDER } from '~/actions'

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_ORDERS:
            return action.payload
        case SUBMIT_ORDER:
        case FETCH_ORDER:
            return { [action.payload.id]: action.payload, ...state }
        case UPDATE_ORDER:
            const actual = state[action.payload.id]
            return assign({}, state, { [action.payload.id]: assign({}, actual, action.payload) })
        case DELETE_ORDER:
            return omit(state, action.payload)
        default:
            return state
    }
}
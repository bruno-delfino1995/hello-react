import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import ReduxPromise from 'redux-promise'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import OrdersPage from '~/pages/orders.page'
import OrderDetails from '~/pages/order-details.page'
import AsListTest from '~/components/as-list-test'

import withRouteParams from '~/utils/components/with-route-params'

import reducers from '~/reducers'

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore)

const mapper = ({ order_id: id }) => ({ id })

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <Switch>
        <Route path="/orders/:order_id/details" component={
          withRouteParams(mapper, OrderDetails)
        } />
        <Route path="/orders" component={OrdersPage} />
        <Route path="/list" component={(props) => <div><AsListTest /><AsListTest /></div>} />
      </Switch>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container')
)

import React, { Component } from 'react'
import { chain } from 'lodash'

export default function AsList(Outer, Inner) {
    return class AsListDecorator extends Component {
        componentWillMount() {
            this.props.index()
        }

        render() {
            const listItems = chain(this.props.data)
                .values()
                .map((item, id) => {
                    return (
                        <Inner key={id} data={item} {...this.props.itemProps()} />
                    )
                })
                .value()

            return (
                <Outer data={this.props.data}>
                    {listItems}
                </Outer>
            )
        }
    }
}
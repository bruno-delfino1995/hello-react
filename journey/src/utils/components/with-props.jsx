import React from 'react'
import { assign } from 'lodash'

export default function WithProps(defaults, Component) {
    return function WithPropsDecorator(props) {
        return (
            <Component {...assign({}, defaults, props)} />
        )
    }
}
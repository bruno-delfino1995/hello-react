import React from 'react'

export default function TextArea(field) {
    const hasError = field.meta.touched && field.meta.invalid
    const className = `form-group ${hasError ? 'has-danger' : ''}`

    return (
        <div className="form-check">
            <label className="form-check-label">
                <input id={field.id} className="form-check-input"
                type="checkbox"
                {...field.input}
                />
                {field.label}
            </label>
            <div className="text-help">
                {field.meta.touched ? field.meta.error : ''}
            </div>
        </div>
    )
}
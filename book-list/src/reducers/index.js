import { combineReducers } from 'redux';

import booksReducer from './books.reducer'
import activeBook from './active-book.reducer'

const rootReducer = combineReducers({
  books: booksReducer,
  activeBook: activeBook
});

export default rootReducer;
